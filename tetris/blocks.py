from copy import deepcopy


class Blocks:
    @staticmethod
    def make_block(old_map):
        new_map = deepcopy(old_map)

        ex_block = [
            [".", ".", "b", "."],
            [".", ".", "b", "."],
            [".", "b", "b", "."],
            [".", ".", ".", "."]
        ]

        new_map_length = len(new_map[0])
        block_length = len(ex_block[0])
        start_point = (new_map_length // 2) - (block_length // 2)

        for i in range(1, 1 + block_length):
            for j in range(start_point, start_point + block_length):
                new_map[i][j] = ex_block[i - 1][j - start_point]

        return new_map
