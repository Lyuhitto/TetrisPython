class Rotation:
    @staticmethod
    def clockwise_rotate(a):
        length = len(a)
        b = [["."] * length for i in range(length)]
        for row in range(length):
            for col in range(length):
                b[col][length-1-row] = a[row][col]
        return b
