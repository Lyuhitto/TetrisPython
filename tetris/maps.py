from copy import deepcopy


class Maps:
    @staticmethod
    def make_new_empty_map(a):
        b = deepcopy(a)
        row = len(b)
        col = len(b[0])

        i = 1
        while i < row - 1:
            j = 1
            while j < col - 1:
                b[i][j] = "."
                j += 1
            i += 1

        return b
