from copy import deepcopy


class Move:
    @staticmethod
    def down(screen):
        screen_moved = deepcopy(screen)
        block = "."
        for y, row in enumerate(screen):
            for x, col in enumerate(row):
                if col != "0" and col != "M" and col != ".":
                    screen_moved[y+1][x] = "B"
                    if screen_moved[y][x] == "B":
                        continue
                    screen_moved[y][x] = "."
                    block = col
        for y, row in enumerate(screen_moved):
            for x, col in enumerate(row):
                if col == "B":
                    screen_moved[y][x] = block
        return screen_moved
