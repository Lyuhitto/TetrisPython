import pytest
from tetris import Rotation


@pytest.fixture
def blocks():
    j = [
        [".", ".", "b", "."],
        [".", ".", "b", "."],
        [".", "b", "b", "."],
        [".", ".", ".", "."]
    ]

    z = [
        [".", ".", ".", "."],
        ["b", "b", ".", "."],
        [".", "b", "b", "."],
        [".", ".", ".", "."]
    ]
    return j, z

@pytest.fixture
def blocks_clock():
    j_clock = [
        [".", ".", ".", "."],
        [".", "b", ".", "."],
        [".", "b", "b", "b"],
        [".", ".", ".", "."]
    ]

    z_clock = [
        [".", ".", "b", "."],
        [".", "b", "b", "."],
        [".", "b", ".", "."],
        [".", ".", ".", "."]
    ]

    return j_clock, z_clock

def test_can_rotate_block_clockwise(blocks, blocks_clock):
    j, z = blocks
    j_clock, z_clock = blocks_clock

    result = Rotation.clockwise_rotate(j)
    assert result == j_clock

    result = Rotation.clockwise_rotate(z)
    assert result == z_clock



