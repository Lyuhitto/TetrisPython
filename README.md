[![Build Status](https://travis-ci.org/Gunforge/TetrisPython.svg?branch=master)](https://travis-ci.org/Gunforge/TetrisPython)
![Github Action](https://github.com/Gunforge/TetrisPython/workflows/TetrisPython/badge.svg)


# tdd-tetris
## Prerequisite
```bash
pip install pytest
```

## Run test
```bash
cd $PROJECT_HOME
python -m pytest
```

